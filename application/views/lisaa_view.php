<form action="<?php print site_url() . '/tiedosto/tallenna'; ?>" enctype="multipart/form-data" method="post">
    <h4>Lisää tiedosto</h4>
    <div class="form-group">
        <label>Nimi:</label>
        <input type="text" class="form-control" name="nimi" required>
    </div>
    <div class="form-group">
        <label>Tiedostonimi:</label>
        <input type="text" class="form-control" name="tiedostonimi"> <!--delete-->
        <!--<input name="userfile" class="form-control" type="file">-->
    </div>
    <div class="form-group">
        <label>Kuvaus</label>
        <input type="text" class="form-control" name="kuvaus">
    </div>
    <button class="btn btn-primary">Tallenna</button>
</form>
