<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tehtavalista</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        
        <div class="container-fluid">
            <h3>Tiedostopankki</h3>
            <?php
            $this->load->view($main_content);
            ?>
        </div>
        
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
