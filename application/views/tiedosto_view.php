<div class="col-sm-10"><h4>Tiedostot</h4></div>
<div class="col-sm-2"><a href="<?php print site_url() . '/tiedosto/lisaa'; ?>" class="btn btn-primary">Lisää</a></div>

<table class="table">
    <tr>
        <th>Nimi</th>
        <th>Tiedosto</th>
        <th>Kuvaus</th>
        <th>Tallennettu</th>
        <th></th>
    </tr>
    
    <?php
    foreach ($tiedostot as $tiedosto) {
        print "<tr>";
        print "<td>$tiedosto->nimi</td>";
        print "<td>$tiedosto->tiedostonimi</td>";
        print "<td>$tiedosto->kuvaus</td>";
        print "<td>$tiedosto->tallennettu</td>";
        print "<td>" . anchor("tiedosto/poista/$tiedosto->id", "Poista") . "</td>";
        print "</tr>";
    };
    ?>
</table>
<?php echo $this->pagination->create_links(); ?>