<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function hae_kaikki($limit = NULL, $offset = NULL) {
        $this->db->limit($limit,$offset);
        $query = $this->db->get('tiedosto');
        return $query->result();
    }
    
    public function lisaa($data) {
        $this->db->insert('tiedosto', $data);
        return $this->db->insert_id();
    }
    
    private function tallenna_tiedosto() {
        $polku = $this->config->item('upload_path') . '/';
        $tiedosto_nimi = $this->tallenna_tiedosto($polku);
        $data['tiedostonimi'] = $tiedosto_nimi;
    }
    
    public function poista($id) {
        $this->db->where('id',$id);
        $this->db->delete('tiedosto');
    }
    
    public function laske_tiedostot() {
            return $this->db->count_all_results('tiedosto');
        }
}