<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('tiedosto_model');
        $this->load->library('pagination');
    }
    
    public function index() {
        $tiedostoja_sivulla = 3;
        $data['tiedostot'] = $this->tiedosto_model->hae_kaikki($tiedostoja_sivulla, $this->uri->segment(3));
        
        $data['sivutuksen_kohta'] = 0;
        
        if($this->uri->segment(3)) {
            $data["sivutuksen_kohta"] = $this->uri->segment(3);
        }
        
        $config['base_url'] = site_url('tiedosto/index');
        $config['total_rows'] = $this->tiedosto_model->laske_tiedostot();
        $config['per_page'] = $tiedostoja_sivulla;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        $data['main_content'] = 'tiedosto_view';
        $this->load->view('template', $data);
    }
    
    public function lisaa() {
        $data['main_content'] = 'lisaa_view';
        $this->load->view('template', $data);
    }
    
    public function tallenna() {
        $data = array(
            'nimi' => $this->input->post('nimi'),
            'tiedostonimi' => $this->input->post('tiedostonimi'), //delete
            'kuvaus' => $this->input->post('kuvaus')
        );
        $this->tiedosto_model->lisaa($data);
        $this->index();
    }
    
    public function poista($id) {
        $this->tiedosto_model->poista(intval($id));
        redirect('tiedosto/index');
    }
}