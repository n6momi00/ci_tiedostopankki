/**/
drop database if exists ci_tiedostopankki;

create database ci_tiedostopankki;

use ci_tiedostopankki;

create table tiedosto(
    id int primary key auto_increment,
    nimi varchar(50) not null,
    tiedostonimi varchar(50) not null,
    kuvaus varchar(255) not null,
    tallennettu timestamp default current_timestamp
);